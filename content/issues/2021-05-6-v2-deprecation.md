---
title: V2 Onion Services deprecation
date: 2021-05-06 16:45:00
resolved: false
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - v2 Onion Services
section: issue
---

**If you are an onion site administrator, you must upgrade to v3 onion services as soon as possible.**

As we [announced last year](https://blog.torproject.org/v2-deprecation-timeline), v2 onion services will be deprecated and obsolete in Tor 0.4.6.x. As of April 2021, Tor Browser Alpha uses this version of Tor and v2 addresses no longer work in this and future versions of Tor Browser Alpha.

When Tor Browser stable moves to Tor 0.4.6.x in October 2021, v2 onion addresses will be completely unreachable.

Why are we deprecating v2 onion services? Safety. Technologies used in v2 onion services are vulnerable to different kinds of attacks, and v2 onion services are no longer being developed or maintained. The new version of onion services provides improved encryption and enhanced privacy for administrators and users.

It's critical that onion service administrators migrate to v3 onion services and work to inform users about this change as soon as possible.

[Read more about the deprecation on our blog](https://blog.torproject.org/v2-deprecation-timeline).
